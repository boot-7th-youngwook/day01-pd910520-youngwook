package com.posco.java.board.entity;

import java.util.Date;
import java.util.UUID;

import com.google.gson.Gson;

public class Comment {
	//
	private String commentId;
	private String authorName;
	private Date regDate;
	private String comments;
	private String articleId;
	
	public Comment() {
		//
		this.commentId = UUID.randomUUID().toString();
	}

	public String getCommentId() {
		return commentId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}
	
	public static Comment sampleComment() {
		//
		Comment sample = new Comment();
		sample.setAuthorName("Kim");
		sample.setComments("Test Comment~~");
		sample.setRegDate(new Date(System.currentTimeMillis()));
		return sample;
	}
	
	public static void main(String [] args) {
		//
		System.out.println(new Gson().toJson(Comment.sampleComment()));
	}

}
