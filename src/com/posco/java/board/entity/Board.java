package com.posco.java.board.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;
import com.posco.java.board.store.repository.BoardRepository;

public class Board {
	//
	private String boardId;
	private String boardName;
	private Date createdDate;
	private String creatorName;
	private List<Article> articles;

	public Board() {
		//
		this.boardId = UUID.randomUUID().toString();
		this.articles = new ArrayList<>();
	}

	public String getBoardId() {
		return boardId;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public static Board sampleBoard() {
		//
		Board sample = new Board();
		sample.setBoardName("Test Board!");
		sample.setCreatedDate(new Date(System.currentTimeMillis()));
		sample.setCreatorName("Choi");

		Article article = Article.sampleArticle();
		article.setBoardId(sample.getBoardId());
		sample.getArticles().add(article);

		return sample;
	}
	
	public static void main(String[] args) {
		//
		BoardRepository repo = BoardRepository.getInstance();
		repo.insert(Board.sampleBoard());
	}

}
