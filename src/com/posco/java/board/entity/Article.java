package com.posco.java.board.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;

public class Article {
	//
	private String articleId;
	private String title;
	private String authorName;
	private Date regDate;
	private String contents;
	private String boardId;
	private List<Comment> comments;
	
	public Article() {
		//
		this.articleId = UUID.randomUUID().toString();
		this.comments = new ArrayList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getArticleId() {
		return articleId;
	}
	
	public static Article sampleArticle() {
		//
		Article sample = new Article();
		sample.setTitle("Test Article");
		sample.setContents("Test article contents");
		
		Comment comment = Comment.sampleComment();
		comment.setArticleId(sample.getArticleId());
		sample.getComments().add(comment);
		
		sample.setAuthorName("Song");
		sample.setRegDate(new Date(System.currentTimeMillis()));
		return sample;
	}
	
	public static void main(String[] args) {
		//
		System.out.println(new Gson().toJson(Article.sampleArticle()));
	}

}
