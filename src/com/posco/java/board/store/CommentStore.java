package com.posco.java.board.store;

import java.util.List;

import com.posco.java.board.entity.Comment;

public interface CommentStore {

	String create(Comment comment);
	List<Comment> retrieveAll(String articleId);
	
	void delete(String commentId);
}
