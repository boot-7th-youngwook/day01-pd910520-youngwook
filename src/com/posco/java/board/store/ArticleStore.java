package com.posco.java.board.store;

import java.util.List;

import com.posco.java.board.entity.Article;

public interface ArticleStore {

	String create(Article article);
	Article retrieve(String articleId);
	List<Article> retrieveAll(String boardId);
	
	void update(Article article);
	void delete(String articleId);
}
