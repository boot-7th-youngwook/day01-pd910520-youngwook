package com.posco.java.board.store.logic;

import java.util.List;

import com.posco.java.board.entity.Board;
import com.posco.java.board.store.BoardStore;
import com.posco.java.board.store.repository.BoardRepository;

public class BoardMemStore implements BoardStore {

	private BoardRepository boardRepo;
	
	public BoardMemStore() {
		this.boardRepo = BoardRepository.getInstance();
	}
	@Override
	public String create(Board board) {
		return this.boardRepo.insert(board);
	}

	@Override
	public Board retrieve(String boardId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Board> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Board board) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(String boardId) {
		// TODO Auto-generated method stub

	}

}
