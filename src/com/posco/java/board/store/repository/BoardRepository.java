package com.posco.java.board.store.repository;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.posco.java.board.entity.Board;

public class BoardRepository {

	private static final String FILE_NAME = "resource/board.json";
	
	private static BoardRepository instance;
	
	private Map<String,Board> boardMap;
	
	private BoardRepository() {
		this.boardMap = new HashMap<String,Board>();
		readFromFile();
	}
	
	public static BoardRepository getInstance() {
		if(instance == null) {
			instance = new BoardRepository();
		}
		return instance;
	}
	
	public String insert(Board board) {
		this.boardMap.put(board.getBoardId(),board);
		saveToFile();
		return board.getBoardId();
	}
	
	public List<Board> selectAll(){
		 
		return this.boardMap.values().stream()
									 .collect(Collectors.toList());
	}
	
	public Board selectById(String boardId) {
		return boardMap.values().stream()
								.filter(board -> board.getBoardId().equals(boardId))
								.findFirst().get();
//		return boardMap.get(boardId);
	}
	
	public void update(Board board) {
		
	}
	
	public void delete(String boardId) {
		Board foundBoard = boardMap.values().stream()
											.filter(board -> board.getBoardId().equals(boardId))
											.findFirst().get();
		
//		Board foundBoard = boardMap.get(boardId);
		if(foundBoard == null) {
			return;
		}
		boardMap.remove(boardId);
		saveToFile();
	}
	
	private void saveToFile() {
		try(FileWriter fileWriter = new FileWriter(FILE_NAME)){
			new Gson().toJson(boardMap.values(),fileWriter);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void readFromFile() {
		List<Board> boards = null;
		try(FileReader fileReader = new FileReader(FILE_NAME)){
			boards = new Gson().fromJson(fileReader, new TypeToken<List<Board>>() {}.getType());
			
			if(boards == null || boards.isEmpty()) {
				return;
			}
			boardMap.clear();
			boards.stream().forEach(board -> boardMap.put(board.getBoardId(),board));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
